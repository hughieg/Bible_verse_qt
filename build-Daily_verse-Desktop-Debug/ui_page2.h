/********************************************************************************
** Form generated from reading UI file 'page2.ui'
**
** Created by: Qt User Interface Compiler version 6.6.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PAGE2_H
#define UI_PAGE2_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_page2
{
public:
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QPushButton *pushButton;
    QTextEdit *page2_quote3_text3;
    QTextEdit *page2_quote2_text;
    QTextEdit *page2_quote2_text2;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton_to_page3;
    QPushButton *backButton_main;
    QWidget *gridLayoutWidget_2;
    QGridLayout *gridLayout_2;
    QPushButton *pushButton_4;
    QTextEdit *page2_quote6_text;
    QTextEdit *page2_quote4_text4;
    QPushButton *pushButton_5;
    QTextEdit *page2_quote5_text5;
    QPushButton *pushButton_7;

    void setupUi(QWidget *page2)
    {
        if (page2->objectName().isEmpty())
            page2->setObjectName("page2");
        page2->resize(1280, 720);
        page2->setMaximumSize(QSize(1280, 720));
        page2->setStyleSheet(QString::fromUtf8("background-color: qlineargradient(spread:pad, x1:0.113, y1:0.721591, x2:1, y2:1, stop:0 rgba(0, 0, 0, 255), stop:1 rgba(147, 147, 147, 255));"));
        gridLayoutWidget = new QWidget(page2);
        gridLayoutWidget->setObjectName("gridLayoutWidget");
        gridLayoutWidget->setGeometry(QRect(630, 80, 611, 481));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName("gridLayout");
        gridLayout->setContentsMargins(0, 0, 0, 0);
        pushButton = new QPushButton(gridLayoutWidget);
        pushButton->setObjectName("pushButton");
        pushButton->setStyleSheet(QString::fromUtf8("background-color: rgb(129, 61, 156);\n"
"color: rgb(255, 255, 255);\n"
"\n"
"font: 700 11pt \"Comfortaa\";"));

        gridLayout->addWidget(pushButton, 1, 0, 1, 2);

        page2_quote3_text3 = new QTextEdit(gridLayoutWidget);
        page2_quote3_text3->setObjectName("page2_quote3_text3");
        page2_quote3_text3->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);\n"
"font: 700 11pt \"Comfortaa\";\n"
"background-image: url(:/resources/free-photo-of-steel-wool-photography-with-milky-way.jpeg);\n"
""));
        page2_quote3_text3->setReadOnly(true);

        gridLayout->addWidget(page2_quote3_text3, 4, 0, 1, 1);

        page2_quote2_text = new QTextEdit(gridLayoutWidget);
        page2_quote2_text->setObjectName("page2_quote2_text");
        page2_quote2_text->setStyleSheet(QString::fromUtf8("color: rgb(51, 209, 122);\n"
"font: 700 12pt \"Comfortaa\";\n"
"background-image: url(:/resources/pexels-photo-4737484.jpeg);"));
        page2_quote2_text->setReadOnly(true);

        gridLayout->addWidget(page2_quote2_text, 0, 0, 1, 2);

        page2_quote2_text2 = new QTextEdit(gridLayoutWidget);
        page2_quote2_text2->setObjectName("page2_quote2_text2");
        page2_quote2_text2->setStyleSheet(QString::fromUtf8("color: rgb(236, 231, 49);\n"
"font: 700 12pt \"Comfortaa\";\n"
"background-image: url(:/resources/pexels-photo-172277.jpeg);"));
        page2_quote2_text2->setReadOnly(true);

        gridLayout->addWidget(page2_quote2_text2, 2, 0, 1, 1);

        pushButton_2 = new QPushButton(gridLayoutWidget);
        pushButton_2->setObjectName("pushButton_2");
        pushButton_2->setStyleSheet(QString::fromUtf8("background-color: rgb(129, 61, 156);\n"
"color: rgb(255, 255, 255);\n"
"\n"
"font: 700 11pt \"Comfortaa\";"));

        gridLayout->addWidget(pushButton_2, 3, 0, 1, 1);

        pushButton_3 = new QPushButton(gridLayoutWidget);
        pushButton_3->setObjectName("pushButton_3");
        pushButton_3->setStyleSheet(QString::fromUtf8("background-color: rgb(129, 61, 156);\n"
"color: rgb(255, 255, 255);\n"
"\n"
"font: 700 11pt \"Comfortaa\";"));

        gridLayout->addWidget(pushButton_3, 5, 0, 1, 1);

        pushButton_to_page3 = new QPushButton(page2);
        pushButton_to_page3->setObjectName("pushButton_to_page3");
        pushButton_to_page3->setGeometry(QRect(660, 600, 91, 41));
        pushButton_to_page3->setStyleSheet(QString::fromUtf8("background-color: rgb(129, 61, 156);\n"
"color: rgb(51, 209, 122);\n"
"font: 700 15pt \"Comfortaa\";"));
        backButton_main = new QPushButton(page2);
        backButton_main->setObjectName("backButton_main");
        backButton_main->setGeometry(QRect(510, 600, 91, 41));
        backButton_main->setStyleSheet(QString::fromUtf8("background-color: rgb(129, 61, 156);\n"
"color: rgb(51, 209, 122);\n"
"font: 700 15pt \"Comfortaa\";"));
        gridLayoutWidget_2 = new QWidget(page2);
        gridLayoutWidget_2->setObjectName("gridLayoutWidget_2");
        gridLayoutWidget_2->setGeometry(QRect(20, 80, 601, 481));
        gridLayout_2 = new QGridLayout(gridLayoutWidget_2);
        gridLayout_2->setObjectName("gridLayout_2");
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        pushButton_4 = new QPushButton(gridLayoutWidget_2);
        pushButton_4->setObjectName("pushButton_4");
        pushButton_4->setStyleSheet(QString::fromUtf8("background-color: rgb(129, 61, 156);\n"
"color: rgb(255, 255, 255);\n"
"\n"
"font: 700 11pt \"Comfortaa\";"));

        gridLayout_2->addWidget(pushButton_4, 1, 0, 1, 1);

        page2_quote6_text = new QTextEdit(gridLayoutWidget_2);
        page2_quote6_text->setObjectName("page2_quote6_text");
        page2_quote6_text->setStyleSheet(QString::fromUtf8("color: rgb(246, 245, 244);\n"
"font: 700 11pt \"Comfortaa\";\n"
"background-image: url(:/resources/pexels-photo-2258536.jpeg);"));
        page2_quote6_text->setReadOnly(true);

        gridLayout_2->addWidget(page2_quote6_text, 4, 0, 1, 1);

        page2_quote4_text4 = new QTextEdit(gridLayoutWidget_2);
        page2_quote4_text4->setObjectName("page2_quote4_text4");
        page2_quote4_text4->setStyleSheet(QString::fromUtf8("color: rgb(253, 193, 241);\n"
"font: 700 12pt \"Comfortaa\";\n"
"background-image: url(:/resources/pexels-photo-235985.jpeg);"));
        page2_quote4_text4->setReadOnly(true);

        gridLayout_2->addWidget(page2_quote4_text4, 0, 0, 1, 1);

        pushButton_5 = new QPushButton(gridLayoutWidget_2);
        pushButton_5->setObjectName("pushButton_5");
        pushButton_5->setStyleSheet(QString::fromUtf8("background-color: rgb(129, 61, 156);\n"
"color: rgb(255, 255, 255);\n"
"\n"
"font: 700 11pt \"Comfortaa\";"));

        gridLayout_2->addWidget(pushButton_5, 3, 0, 1, 1);

        page2_quote5_text5 = new QTextEdit(gridLayoutWidget_2);
        page2_quote5_text5->setObjectName("page2_quote5_text5");
        page2_quote5_text5->setStyleSheet(QString::fromUtf8("color: rgb(134, 94, 60);\n"
"font: 700 12pt \"Comfortaa\";\n"
"background-image: url(:/resources/pexels-photo-2529973.jpeg);"));
        page2_quote5_text5->setReadOnly(true);

        gridLayout_2->addWidget(page2_quote5_text5, 2, 0, 1, 1);

        pushButton_7 = new QPushButton(gridLayoutWidget_2);
        pushButton_7->setObjectName("pushButton_7");
        pushButton_7->setStyleSheet(QString::fromUtf8("background-color: rgb(129, 61, 156);\n"
"color: rgb(255, 255, 255);\n"
"\n"
"font: 700 11pt \"Comfortaa\";"));

        gridLayout_2->addWidget(pushButton_7, 5, 0, 1, 1);


        retranslateUi(page2);

        QMetaObject::connectSlotsByName(page2);
    } // setupUi

    void retranslateUi(QWidget *page2)
    {
        page2->setWindowTitle(QCoreApplication::translate("page2", "Form", nullptr));
        pushButton->setText(QCoreApplication::translate("page2", "Read 1 Corinthians Chapter 15", nullptr));
        pushButton_2->setText(QCoreApplication::translate("page2", "Read Matthew Chapter 20", nullptr));
        pushButton_3->setText(QCoreApplication::translate("page2", "Read Hebrews Chapter 10", nullptr));
        pushButton_to_page3->setText(QCoreApplication::translate("page2", "Next", nullptr));
        backButton_main->setText(QCoreApplication::translate("page2", "Back", nullptr));
        pushButton_4->setText(QCoreApplication::translate("page2", "Read 1 John Chapter 1", nullptr));
        pushButton_5->setText(QCoreApplication::translate("page2", "Read Philipians Chapter 4", nullptr));
        pushButton_7->setText(QCoreApplication::translate("page2", "Read Psalms 27", nullptr));
    } // retranslateUi

};

namespace Ui {
    class page2: public Ui_page2 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PAGE2_H
