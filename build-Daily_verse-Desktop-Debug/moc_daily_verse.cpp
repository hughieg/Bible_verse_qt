/****************************************************************************
** Meta object code from reading C++ file 'daily_verse.h'
**
** Created by: The Qt Meta Object Compiler version 68 (Qt 6.6.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Daily_verse/daily_verse.h"
#include <QtGui/qtextcursor.h>
#include <QtCore/qmetatype.h>

#if __has_include(<QtCore/qtmochelpers.h>)
#include <QtCore/qtmochelpers.h>
#else
QT_BEGIN_MOC_NAMESPACE
#endif


#include <memory>

#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'daily_verse.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 68
#error "This file was generated using the moc from 6.6.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

#ifndef Q_CONSTINIT
#define Q_CONSTINIT
#endif

QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
QT_WARNING_DISABLE_GCC("-Wuseless-cast")
namespace {

#ifdef QT_MOC_HAS_STRINGDATA
struct qt_meta_stringdata_CLASSDaily_verseENDCLASS_t {};
static constexpr auto qt_meta_stringdata_CLASSDaily_verseENDCLASS = QtMocHelpers::stringData(
    "Daily_verse",
    "myfunction",
    "",
    "quote2",
    "quote3",
    "quote4",
    "quote5",
    "quote6",
    "mylabel",
    "on_pushButton_clicked",
    "on_Proverbs_12_clicked",
    "on_Romans_12_clicked",
    "on_Proverbs_7_clicked",
    "on_goto_page2_clicked",
    "on_Romans_13_clicked",
    "on_Romans_14_clicked",
    "on_pushButton_5_clicked",
    "on_pushButton_quit_clicked",
    "on_pushButton_12_clicked"
);
#else  // !QT_MOC_HAS_STRING_DATA
struct qt_meta_stringdata_CLASSDaily_verseENDCLASS_t {
    uint offsetsAndSizes[38];
    char stringdata0[12];
    char stringdata1[11];
    char stringdata2[1];
    char stringdata3[7];
    char stringdata4[7];
    char stringdata5[7];
    char stringdata6[7];
    char stringdata7[7];
    char stringdata8[8];
    char stringdata9[22];
    char stringdata10[23];
    char stringdata11[21];
    char stringdata12[22];
    char stringdata13[22];
    char stringdata14[21];
    char stringdata15[21];
    char stringdata16[24];
    char stringdata17[27];
    char stringdata18[25];
};
#define QT_MOC_LITERAL(ofs, len) \
    uint(sizeof(qt_meta_stringdata_CLASSDaily_verseENDCLASS_t::offsetsAndSizes) + ofs), len 
Q_CONSTINIT static const qt_meta_stringdata_CLASSDaily_verseENDCLASS_t qt_meta_stringdata_CLASSDaily_verseENDCLASS = {
    {
        QT_MOC_LITERAL(0, 11),  // "Daily_verse"
        QT_MOC_LITERAL(12, 10),  // "myfunction"
        QT_MOC_LITERAL(23, 0),  // ""
        QT_MOC_LITERAL(24, 6),  // "quote2"
        QT_MOC_LITERAL(31, 6),  // "quote3"
        QT_MOC_LITERAL(38, 6),  // "quote4"
        QT_MOC_LITERAL(45, 6),  // "quote5"
        QT_MOC_LITERAL(52, 6),  // "quote6"
        QT_MOC_LITERAL(59, 7),  // "mylabel"
        QT_MOC_LITERAL(67, 21),  // "on_pushButton_clicked"
        QT_MOC_LITERAL(89, 22),  // "on_Proverbs_12_clicked"
        QT_MOC_LITERAL(112, 20),  // "on_Romans_12_clicked"
        QT_MOC_LITERAL(133, 21),  // "on_Proverbs_7_clicked"
        QT_MOC_LITERAL(155, 21),  // "on_goto_page2_clicked"
        QT_MOC_LITERAL(177, 20),  // "on_Romans_13_clicked"
        QT_MOC_LITERAL(198, 20),  // "on_Romans_14_clicked"
        QT_MOC_LITERAL(219, 23),  // "on_pushButton_5_clicked"
        QT_MOC_LITERAL(243, 26),  // "on_pushButton_quit_clicked"
        QT_MOC_LITERAL(270, 24)   // "on_pushButton_12_clicked"
    },
    "Daily_verse",
    "myfunction",
    "",
    "quote2",
    "quote3",
    "quote4",
    "quote5",
    "quote6",
    "mylabel",
    "on_pushButton_clicked",
    "on_Proverbs_12_clicked",
    "on_Romans_12_clicked",
    "on_Proverbs_7_clicked",
    "on_goto_page2_clicked",
    "on_Romans_13_clicked",
    "on_Romans_14_clicked",
    "on_pushButton_5_clicked",
    "on_pushButton_quit_clicked",
    "on_pushButton_12_clicked"
};
#undef QT_MOC_LITERAL
#endif // !QT_MOC_HAS_STRING_DATA
} // unnamed namespace

Q_CONSTINIT static const uint qt_meta_data_CLASSDaily_verseENDCLASS[] = {

 // content:
      12,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags, initial metatype offsets
       1,    0,  116,    2, 0x0a,    1 /* Public */,
       3,    0,  117,    2, 0x0a,    2 /* Public */,
       4,    0,  118,    2, 0x0a,    3 /* Public */,
       5,    0,  119,    2, 0x0a,    4 /* Public */,
       6,    0,  120,    2, 0x0a,    5 /* Public */,
       7,    0,  121,    2, 0x0a,    6 /* Public */,
       8,    0,  122,    2, 0x0a,    7 /* Public */,
       9,    0,  123,    2, 0x08,    8 /* Private */,
      10,    0,  124,    2, 0x08,    9 /* Private */,
      11,    0,  125,    2, 0x08,   10 /* Private */,
      12,    0,  126,    2, 0x08,   11 /* Private */,
      13,    0,  127,    2, 0x08,   12 /* Private */,
      14,    0,  128,    2, 0x08,   13 /* Private */,
      15,    0,  129,    2, 0x08,   14 /* Private */,
      16,    0,  130,    2, 0x08,   15 /* Private */,
      17,    0,  131,    2, 0x08,   16 /* Private */,
      18,    0,  132,    2, 0x08,   17 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

Q_CONSTINIT const QMetaObject Daily_verse::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_CLASSDaily_verseENDCLASS.offsetsAndSizes,
    qt_meta_data_CLASSDaily_verseENDCLASS,
    qt_static_metacall,
    nullptr,
    qt_incomplete_metaTypeArray<qt_meta_stringdata_CLASSDaily_verseENDCLASS_t,
        // Q_OBJECT / Q_GADGET
        QtPrivate::TypeAndForceComplete<Daily_verse, std::true_type>,
        // method 'myfunction'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'quote2'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'quote3'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'quote4'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'quote5'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'quote6'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'mylabel'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_pushButton_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_Proverbs_12_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_Romans_12_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_Proverbs_7_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_goto_page2_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_Romans_13_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_Romans_14_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_pushButton_5_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_pushButton_quit_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_pushButton_12_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>
    >,
    nullptr
} };

void Daily_verse::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Daily_verse *>(_o);
        (void)_t;
        switch (_id) {
        case 0: _t->myfunction(); break;
        case 1: _t->quote2(); break;
        case 2: _t->quote3(); break;
        case 3: _t->quote4(); break;
        case 4: _t->quote5(); break;
        case 5: _t->quote6(); break;
        case 6: _t->mylabel(); break;
        case 7: _t->on_pushButton_clicked(); break;
        case 8: _t->on_Proverbs_12_clicked(); break;
        case 9: _t->on_Romans_12_clicked(); break;
        case 10: _t->on_Proverbs_7_clicked(); break;
        case 11: _t->on_goto_page2_clicked(); break;
        case 12: _t->on_Romans_13_clicked(); break;
        case 13: _t->on_Romans_14_clicked(); break;
        case 14: _t->on_pushButton_5_clicked(); break;
        case 15: _t->on_pushButton_quit_clicked(); break;
        case 16: _t->on_pushButton_12_clicked(); break;
        default: ;
        }
    }
    (void)_a;
}

const QMetaObject *Daily_verse::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Daily_verse::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CLASSDaily_verseENDCLASS.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int Daily_verse::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 17)
            *reinterpret_cast<QMetaType *>(_a[0]) = QMetaType();
        _id -= 17;
    }
    return _id;
}
QT_WARNING_POP
