/****************************************************************************
** Meta object code from reading C++ file 'page2.h'
**
** Created by: The Qt Meta Object Compiler version 68 (Qt 6.6.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Daily_verse/page2.h"
#include <QtGui/qtextcursor.h>
#include <QtCore/qmetatype.h>

#if __has_include(<QtCore/qtmochelpers.h>)
#include <QtCore/qtmochelpers.h>
#else
QT_BEGIN_MOC_NAMESPACE
#endif


#include <memory>

#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'page2.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 68
#error "This file was generated using the moc from 6.6.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

#ifndef Q_CONSTINIT
#define Q_CONSTINIT
#endif

QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
QT_WARNING_DISABLE_GCC("-Wuseless-cast")
namespace {

#ifdef QT_MOC_HAS_STRINGDATA
struct qt_meta_stringdata_CLASSpage2ENDCLASS_t {};
static constexpr auto qt_meta_stringdata_CLASSpage2ENDCLASS = QtMocHelpers::stringData(
    "page2",
    "page2_quote1",
    "",
    "page2_quote2",
    "page2_quote3",
    "page2_quote4",
    "page2_quote5",
    "page2_quote6",
    "on_backButton_main_clicked",
    "on_pushButton_clicked",
    "on_pushButton_2_clicked",
    "on_pushButton_3_clicked",
    "on_pushButton_4_clicked",
    "on_pushButton_5_clicked",
    "on_pushButton_7_clicked",
    "on_pushButton_6_clicked",
    "on_pushButton_to_page3_clicked"
);
#else  // !QT_MOC_HAS_STRING_DATA
struct qt_meta_stringdata_CLASSpage2ENDCLASS_t {
    uint offsetsAndSizes[34];
    char stringdata0[6];
    char stringdata1[13];
    char stringdata2[1];
    char stringdata3[13];
    char stringdata4[13];
    char stringdata5[13];
    char stringdata6[13];
    char stringdata7[13];
    char stringdata8[27];
    char stringdata9[22];
    char stringdata10[24];
    char stringdata11[24];
    char stringdata12[24];
    char stringdata13[24];
    char stringdata14[24];
    char stringdata15[24];
    char stringdata16[31];
};
#define QT_MOC_LITERAL(ofs, len) \
    uint(sizeof(qt_meta_stringdata_CLASSpage2ENDCLASS_t::offsetsAndSizes) + ofs), len 
Q_CONSTINIT static const qt_meta_stringdata_CLASSpage2ENDCLASS_t qt_meta_stringdata_CLASSpage2ENDCLASS = {
    {
        QT_MOC_LITERAL(0, 5),  // "page2"
        QT_MOC_LITERAL(6, 12),  // "page2_quote1"
        QT_MOC_LITERAL(19, 0),  // ""
        QT_MOC_LITERAL(20, 12),  // "page2_quote2"
        QT_MOC_LITERAL(33, 12),  // "page2_quote3"
        QT_MOC_LITERAL(46, 12),  // "page2_quote4"
        QT_MOC_LITERAL(59, 12),  // "page2_quote5"
        QT_MOC_LITERAL(72, 12),  // "page2_quote6"
        QT_MOC_LITERAL(85, 26),  // "on_backButton_main_clicked"
        QT_MOC_LITERAL(112, 21),  // "on_pushButton_clicked"
        QT_MOC_LITERAL(134, 23),  // "on_pushButton_2_clicked"
        QT_MOC_LITERAL(158, 23),  // "on_pushButton_3_clicked"
        QT_MOC_LITERAL(182, 23),  // "on_pushButton_4_clicked"
        QT_MOC_LITERAL(206, 23),  // "on_pushButton_5_clicked"
        QT_MOC_LITERAL(230, 23),  // "on_pushButton_7_clicked"
        QT_MOC_LITERAL(254, 23),  // "on_pushButton_6_clicked"
        QT_MOC_LITERAL(278, 30)   // "on_pushButton_to_page3_clicked"
    },
    "page2",
    "page2_quote1",
    "",
    "page2_quote2",
    "page2_quote3",
    "page2_quote4",
    "page2_quote5",
    "page2_quote6",
    "on_backButton_main_clicked",
    "on_pushButton_clicked",
    "on_pushButton_2_clicked",
    "on_pushButton_3_clicked",
    "on_pushButton_4_clicked",
    "on_pushButton_5_clicked",
    "on_pushButton_7_clicked",
    "on_pushButton_6_clicked",
    "on_pushButton_to_page3_clicked"
};
#undef QT_MOC_LITERAL
#endif // !QT_MOC_HAS_STRING_DATA
} // unnamed namespace

Q_CONSTINIT static const uint qt_meta_data_CLASSpage2ENDCLASS[] = {

 // content:
      12,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags, initial metatype offsets
       1,    0,  104,    2, 0x0a,    1 /* Public */,
       3,    0,  105,    2, 0x0a,    2 /* Public */,
       4,    0,  106,    2, 0x0a,    3 /* Public */,
       5,    0,  107,    2, 0x0a,    4 /* Public */,
       6,    0,  108,    2, 0x0a,    5 /* Public */,
       7,    0,  109,    2, 0x0a,    6 /* Public */,
       8,    0,  110,    2, 0x08,    7 /* Private */,
       9,    0,  111,    2, 0x08,    8 /* Private */,
      10,    0,  112,    2, 0x08,    9 /* Private */,
      11,    0,  113,    2, 0x08,   10 /* Private */,
      12,    0,  114,    2, 0x08,   11 /* Private */,
      13,    0,  115,    2, 0x08,   12 /* Private */,
      14,    0,  116,    2, 0x08,   13 /* Private */,
      15,    0,  117,    2, 0x08,   14 /* Private */,
      16,    0,  118,    2, 0x08,   15 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

Q_CONSTINIT const QMetaObject page2::staticMetaObject = { {
    QMetaObject::SuperData::link<QWidget::staticMetaObject>(),
    qt_meta_stringdata_CLASSpage2ENDCLASS.offsetsAndSizes,
    qt_meta_data_CLASSpage2ENDCLASS,
    qt_static_metacall,
    nullptr,
    qt_incomplete_metaTypeArray<qt_meta_stringdata_CLASSpage2ENDCLASS_t,
        // Q_OBJECT / Q_GADGET
        QtPrivate::TypeAndForceComplete<page2, std::true_type>,
        // method 'page2_quote1'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'page2_quote2'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'page2_quote3'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'page2_quote4'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'page2_quote5'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'page2_quote6'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_backButton_main_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_pushButton_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_pushButton_2_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_pushButton_3_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_pushButton_4_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_pushButton_5_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_pushButton_7_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_pushButton_6_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_pushButton_to_page3_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>
    >,
    nullptr
} };

void page2::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<page2 *>(_o);
        (void)_t;
        switch (_id) {
        case 0: _t->page2_quote1(); break;
        case 1: _t->page2_quote2(); break;
        case 2: _t->page2_quote3(); break;
        case 3: _t->page2_quote4(); break;
        case 4: _t->page2_quote5(); break;
        case 5: _t->page2_quote6(); break;
        case 6: _t->on_backButton_main_clicked(); break;
        case 7: _t->on_pushButton_clicked(); break;
        case 8: _t->on_pushButton_2_clicked(); break;
        case 9: _t->on_pushButton_3_clicked(); break;
        case 10: _t->on_pushButton_4_clicked(); break;
        case 11: _t->on_pushButton_5_clicked(); break;
        case 12: _t->on_pushButton_7_clicked(); break;
        case 13: _t->on_pushButton_6_clicked(); break;
        case 14: _t->on_pushButton_to_page3_clicked(); break;
        default: ;
        }
    }
    (void)_a;
}

const QMetaObject *page2::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *page2::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CLASSpage2ENDCLASS.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int page2::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 15)
            *reinterpret_cast<QMetaType *>(_a[0]) = QMetaType();
        _id -= 15;
    }
    return _id;
}
QT_WARNING_POP
