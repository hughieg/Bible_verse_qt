/****************************************************************************
** Meta object code from reading C++ file 'page3.h'
**
** Created by: The Qt Meta Object Compiler version 68 (Qt 6.6.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Daily_verse/page3.h"
#include <QtGui/qtextcursor.h>
#include <QtCore/qmetatype.h>

#if __has_include(<QtCore/qtmochelpers.h>)
#include <QtCore/qtmochelpers.h>
#else
QT_BEGIN_MOC_NAMESPACE
#endif


#include <memory>

#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'page3.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 68
#error "This file was generated using the moc from 6.6.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

#ifndef Q_CONSTINIT
#define Q_CONSTINIT
#endif

QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
QT_WARNING_DISABLE_GCC("-Wuseless-cast")
namespace {

#ifdef QT_MOC_HAS_STRINGDATA
struct qt_meta_stringdata_CLASSpage3ENDCLASS_t {};
static constexpr auto qt_meta_stringdata_CLASSpage3ENDCLASS = QtMocHelpers::stringData(
    "page3",
    "page3_quote1",
    "",
    "page3_quote2",
    "page3_quote3",
    "page3_quote4",
    "page3_quote5",
    "page3_quote6",
    "on_back_to_page2_clicked"
);
#else  // !QT_MOC_HAS_STRING_DATA
struct qt_meta_stringdata_CLASSpage3ENDCLASS_t {
    uint offsetsAndSizes[18];
    char stringdata0[6];
    char stringdata1[13];
    char stringdata2[1];
    char stringdata3[13];
    char stringdata4[13];
    char stringdata5[13];
    char stringdata6[13];
    char stringdata7[13];
    char stringdata8[25];
};
#define QT_MOC_LITERAL(ofs, len) \
    uint(sizeof(qt_meta_stringdata_CLASSpage3ENDCLASS_t::offsetsAndSizes) + ofs), len 
Q_CONSTINIT static const qt_meta_stringdata_CLASSpage3ENDCLASS_t qt_meta_stringdata_CLASSpage3ENDCLASS = {
    {
        QT_MOC_LITERAL(0, 5),  // "page3"
        QT_MOC_LITERAL(6, 12),  // "page3_quote1"
        QT_MOC_LITERAL(19, 0),  // ""
        QT_MOC_LITERAL(20, 12),  // "page3_quote2"
        QT_MOC_LITERAL(33, 12),  // "page3_quote3"
        QT_MOC_LITERAL(46, 12),  // "page3_quote4"
        QT_MOC_LITERAL(59, 12),  // "page3_quote5"
        QT_MOC_LITERAL(72, 12),  // "page3_quote6"
        QT_MOC_LITERAL(85, 24)   // "on_back_to_page2_clicked"
    },
    "page3",
    "page3_quote1",
    "",
    "page3_quote2",
    "page3_quote3",
    "page3_quote4",
    "page3_quote5",
    "page3_quote6",
    "on_back_to_page2_clicked"
};
#undef QT_MOC_LITERAL
#endif // !QT_MOC_HAS_STRING_DATA
} // unnamed namespace

Q_CONSTINIT static const uint qt_meta_data_CLASSpage3ENDCLASS[] = {

 // content:
      12,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags, initial metatype offsets
       1,    0,   56,    2, 0x0a,    1 /* Public */,
       3,    0,   57,    2, 0x0a,    2 /* Public */,
       4,    0,   58,    2, 0x0a,    3 /* Public */,
       5,    0,   59,    2, 0x0a,    4 /* Public */,
       6,    0,   60,    2, 0x0a,    5 /* Public */,
       7,    0,   61,    2, 0x0a,    6 /* Public */,
       8,    0,   62,    2, 0x08,    7 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

Q_CONSTINIT const QMetaObject page3::staticMetaObject = { {
    QMetaObject::SuperData::link<QWidget::staticMetaObject>(),
    qt_meta_stringdata_CLASSpage3ENDCLASS.offsetsAndSizes,
    qt_meta_data_CLASSpage3ENDCLASS,
    qt_static_metacall,
    nullptr,
    qt_incomplete_metaTypeArray<qt_meta_stringdata_CLASSpage3ENDCLASS_t,
        // Q_OBJECT / Q_GADGET
        QtPrivate::TypeAndForceComplete<page3, std::true_type>,
        // method 'page3_quote1'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'page3_quote2'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'page3_quote3'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'page3_quote4'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'page3_quote5'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'page3_quote6'
        QtPrivate::TypeAndForceComplete<void, std::false_type>,
        // method 'on_back_to_page2_clicked'
        QtPrivate::TypeAndForceComplete<void, std::false_type>
    >,
    nullptr
} };

void page3::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<page3 *>(_o);
        (void)_t;
        switch (_id) {
        case 0: _t->page3_quote1(); break;
        case 1: _t->page3_quote2(); break;
        case 2: _t->page3_quote3(); break;
        case 3: _t->page3_quote4(); break;
        case 4: _t->page3_quote5(); break;
        case 5: _t->page3_quote6(); break;
        case 6: _t->on_back_to_page2_clicked(); break;
        default: ;
        }
    }
    (void)_a;
}

const QMetaObject *page3::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *page3::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CLASSpage3ENDCLASS.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int page3::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<QMetaType *>(_a[0]) = QMetaType();
        _id -= 7;
    }
    return _id;
}
QT_WARNING_POP
